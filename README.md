# stack-growth

> A fun experiment on stack growth in C as demonstrated by [Low Level Learning][links.yt.low-level-learning]

[Low Level Learning][links.yt.low-level-learning] has a lot of good content on systems programming. In their recent video [_a strange but powerful
interview question_][links.yt.low-level-learning.vid], they use a little bit of C to investigate the direction of the growth of the stack on a given architecture.

My somewhat-recently renewed love for GNU/Linux and computing in general made this project seem pretty interesting, so
I thought I would mess around with it a bit. It's always fun to write some C, too.

## Development

This project is equipped with a [dev container][links.devcontainers] config that runs
[Ubuntu 22.04.3 LTS (Jammy Jellfyfish)][links.ubuntu-rel-jammy]. It should also run out
of the box on most modern Linux distributions, as its only true dependencies are a C compiler like [GCC][links.gcc] and
[Bash][links.bash] (though `sh` will work). [`Make`][links.make] is also nice, but not required.

## Compiling

Run the [`Makefile`](./Makefile) with `make`. This is also done upon loading the project's dev container.

## Usage

Run the compiled binary with `./run.bash`

<hr>

<sub>Emoji used for repository logo designed by <a href="https://openmoji.org/">OpenMoji</a> – the open-source emoji and icon project. License: CC
BY-SA 4.0</sub>

<hr>

<!-- Links -->

[links.yt.low-level-learning]: https://www.youtube.com/@LowLevelLearning
[links.yt.low-level-learning.vid]: https://www.youtube.com/watch?v=V2h_hJ5MSpY
[links.ubuntu-rel-jammy]: https://releases.ubuntu.com/jammy/
[links.devcontainers]: https://containers.dev/
[links.gcc]: https://gcc.gnu.org/
[links.bash]: https://www.gnu.org/software/bash/
[links.make]: https://www.gnu.org/software/make/
