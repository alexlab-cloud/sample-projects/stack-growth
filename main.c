#include <stdbool.h>
#include <stdio.h>


/*
* Returns: true if stack grows up (positive), false if down (negative)
*/
bool upordown(int *other) {
    /* Declare a variable at function scope, which will have some initialized value */
    int x;

    /* If other is undefined, return the result of this function with the address of x */
    if (!other) {
        return upordown(&x);
    }

    /* Print the value and address of x */
    printf("x:\t\t%d\n", x);
    printf("&x:\t\t%p\n", &x);

    /* Print the value and address of the variable on the original function stack */
    printf("*other:\t\t%d\n", *other);
    printf("other:\t\t%p\n", other);

    /* Return the boolean value that shows whether the stack grows up or down */
    return &x > other;
}


/*
* Passing NULL to upordown results in the function running twice: once to enter the if statement
* (!NULL == true) and the second upon the recursion that passes the address of x to upordown.
*/
int main() {
    printf("Stack growth: [%s]\n", upordown(NULL) ? "up ↑" : "down ↓");
    return 0;
}
